# coding: utf-8
from __future__ import absolute_import
from __future__ import unicode_literals

from .base import CTestCase


class TestConfrontation(CTestCase):

    def _test_vs(self, strat1, strat2, score1, score2, rounds=1):
        program = "\n".join([
            "int main() {{",
            "    weights_t w = weights_new(D, C, P, T);",
            "    size_t* scores = confrontation({}, {}, {}, &w);"
            "    if ((scores[0] == {}) && (scores[1] == {}))"
            "        return 0;"
            "    else"
            "        return 1;"
            "}}"
        ]).format(strat1, strat2, rounds, score1, score2)
        self._test_program(program, exit_code=0)

    def test_gentille_VS_gentille(self):
        self._test_vs(
            "gentille", "gentille",
            "4*C", "4*C",
            rounds=4
        )

    def test_mechante_VS_mechante(self):
        self._test_vs(
            "mechante", "mechante",
            "4*P", "4*P",
            rounds=4
        )

    def test_mechante_VS_gentille(self):
        self._test_vs(
            "mechante", "gentille",
            "4*T", "4*D",
            rounds=4
        )

    def test_donnant_donnant_VS_rancuniere(self):
        self._test_vs(
            "donnant_donnant", "rancuniere",
            "4*C",        "4*C",
            rounds=4
        )

    def test_periodique_mechante_VS_periodique_gentille(self):
        self._test_vs(
            "periodique_mechante", "periodique_gentille",
            "4*T + 2*D", "4*D + 2*T",
            rounds=6
        )

    def test_mefiante_VS_majorite_dur(self):
        self._test_vs(
            "mefiante", "majorite_dur",
            "3*P", "3*P",
            rounds=3
        )

    def test_mefiante_VS_majorite_mou(self):
        self._test_vs(
            "mefiante",            "majorite_mou",
            "3*T + 2*D", "3*D + 2*T",
            rounds=5
        )

    def test_donnant_donnant_dur_VS_sondeur(self):
        self._test_vs(
            "donnant_donnant_dur",          "sondeur",
            "2*D + 3*T + P", "2*T + 3*D + P",
            rounds=6
        )

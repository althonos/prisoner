#coding: utf-8
from __future__ import absolute_import
from __future__ import unicode_literals

import os
import collections
import unittest

import six
import sh
import fs

from .utils import projfs


class CTestCase(unittest.TestCase):

    def _test_program(self, program, exit_code=0, stdout=b""):

        with fs.open_fs('temp://') as temp_fs:

            # put the program text in a file
            for header in projfs.scandir('inc'):
                temp_fs.appendtext('test.c', '#include "{}"\n'.format(header.name))
            temp_fs.appendtext('test.c', '#include <stdlib.h>\n')
            temp_fs.appendtext('test.c', '#include <stdio.h>\n')
            for name, val in [('D', 0), ('T', 5), ('C', 3), ('P', 1)]:
                temp_fs.appendtext('test.c', '#define {} {}\n'.format(name, val))
            temp_fs.appendtext('test.c', program)


            # compile the test program using the shared library
            sh.gcc(
                temp_fs.getsyspath('test.c'),
                "-I{}".format(projfs.getsyspath('inc')),
                "-L{}".format(projfs.getsyspath('build')),
                "-lprisoner", "-lm",
                "-o{}".format(temp_fs.getsyspath("test")),
            )

            # run the test program
            test_binary = sh.Command(temp_fs.getsyspath('test'))
            result = test_binary(
                _ok_code=range(256),
                _env={'LD_LIBRARY_PATH': projfs.getsyspath('build')}
            )

            print(result.stdout)

            self.assertEqual(result.exit_code, exit_code)
            self.assertEqual(result.stdout, stdout)

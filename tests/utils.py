# coding: utf-8
from __future__ import absolute_import
from __future__ import unicode_literals

import os
import configparser

import sh
import fs

from fs.wrap import read_only

# project global variables
projfs = read_only(fs.open_fs(os.path.join(os.path.abspath(__file__), '..', '..')))
config = configparser.ConfigParser()

# read the configuration from the setup.cfg file
#config.read_string(projfs.gettext('setup.cfg'))

# make sure the shared library is built
if not projfs.isfile('/build/libprisoner.so'):
    sh.make("build/libprisoner.so", C=projfs.getsyspath('/'))

# coding: utf-8
from __future__ import absolute_import
from __future__ import unicode_literals

from .base import CTestCase
from .utils import projfs


C = "COOPERATE"
B = "BETRAY"


class TestStrategies(CTestCase):

    def _test_strategy(self, name, expected, self_choices=[], other_choices=[]):
        program = [
            "int main() {",
            "    Choices* self = choices_new();"
            "    Choices* other = choices_new();"
        ]
        program.extend([
	        "    choices_add(self, {});".format(choice)
        		for choice in self_choices
        ])
        program.extend([
        	"    choices_add(other, {});".format(choice)
        		for choice in other_choices
        ])
        program.extend([
			"    int choice = {}(self, other);".format(name),
			"    return (choice == {});".format(expected),
			"}"
		])
        self._test_program("\n".join(program), exit_code=1)


class TestGentille(TestStrategies):

    def test_round0(self):
        self._test_strategy('gentille', C)

    def test_round1(self):
        self._test_strategy('gentille', C, [C], [C])
        self._test_strategy('gentille', C, [C], [B])


class TestMechante(TestStrategies):

    def test_round0(self):
        self._test_strategy('mechante', B)

    def test_round1(self):
        self._test_strategy('mechante', B, [C], [C])
        self._test_strategy('mechante', B, [C], [B])


class TestDonnantDonnant(TestStrategies):

    def test_round0(self):
        self._test_strategy('donnant_donnant', C)

    def test_round1(self):
        self._test_strategy('donnant_donnant', B, self_choices=[C], other_choices=[B])

    def test_round2(self):
        self._test_strategy('donnant_donnant', C, self_choices=[C, C], other_choices=[C, C])
        self._test_strategy('donnant_donnant', C, self_choices=[C, B], other_choices=[B, C])


class TestRancuniere(TestStrategies):

    def test_round0(self):
        self._test_strategy('rancuniere', C)

    def test_round1(self):
        self._test_strategy('rancuniere', C, self_choices=[C], other_choices=[C])

    def test_round2(self):
        self._test_strategy('rancuniere', B, self_choices=[C, C], other_choices=[C, B])
        self._test_strategy('rancuniere', B, self_choices=[C, C, B], other_choices=[C, B, C])


class TestPeriodiqueMechante(TestStrategies):

    def test_round0(self):
        self._test_strategy('periodique_mechante', B)

    def test_round1(self):
        self._test_strategy('periodique_mechante', B, self_choices=[B])

    def test_round2(self):
        self._test_strategy('periodique_mechante', C, self_choices=[B, B])

    def test_round3(self):
        self._test_strategy('periodique_mechante', B, self_choices=[B, B, C])

    def test_round4(self):
        self._test_strategy('periodique_mechante', B, self_choices=[B, B, C, B])

    def test_round5(self):
        self._test_strategy('periodique_mechante', C, self_choices=[B, B, C, B, B])


class TestPeriodiqueGentille(TestStrategies):

    def test_round0(self):
        self._test_strategy('periodique_gentille', C)

    def test_round1(self):
        self._test_strategy('periodique_gentille', C, self_choices=[C])

    def test_round2(self):
        self._test_strategy('periodique_gentille', B, self_choices=[C, C])

    def test_round3(self):
        self._test_strategy('periodique_gentille', C, self_choices=[C, C, B])

    def test_round4(self):
        self._test_strategy('periodique_gentille', C, self_choices=[C, C, B, C])

    def test_round5(self):
        self._test_strategy('periodique_gentille', B, self_choices=[C, C, B, C, C])


class TestMajoriteMou(TestStrategies):

    def test_round0(self):
        self._test_strategy('majorite_mou', C)

    def test_round2(self):
        self._test_strategy('majorite_mou', C, other_choices=[C, B])

    def test_round3(self):
        self._test_strategy('majorite_mou', C, other_choices=[C, C, B])
        self._test_strategy('majorite_mou', B, other_choices=[C, B, B])


class TestMefiante(TestStrategies):

    def test_round0(self):
        self._test_strategy('mefiante', B)

    def test_round2(self):
        self._test_strategy('mefiante', C, self_choices=[C, C], other_choices=[C, C])
        self._test_strategy('mefiante', C, self_choices=[C, B], other_choices=[B, C])

    def test_round1(self):
        self._test_strategy('mefiante', B, self_choices=[C], other_choices=[B])


class TestMajoriteDur(TestStrategies):

    def test_majorite_dur_round0(self):
        self._test_strategy('majorite_dur', B)

    def test_majorite_dur_round3(self):
        self._test_strategy('majorite_dur', C, other_choices=[C, C, B])
        self._test_strategy('majorite_dur', B, other_choices=[C, B, B])

    def test_majorite_dur_round2(self):
        self._test_strategy('majorite_dur', B, other_choices=[C, B])


class TestSondeur(TestStrategies):

    def test_round0(self):
        self._test_strategy('sondeur', B)

    def test_round1(self):
        self._test_strategy('sondeur', C, [B])

    def test_round2(self):
        self._test_strategy('sondeur', C, [B, C])

    def test_round3(self):
        self._test_strategy('sondeur', B, [B, C, C], [C, C, C])
        self._test_strategy('sondeur', C, [B, C, C], [C, B, C])
        self._test_strategy('sondeur', B, [B, C, C], [C, C, B])


class TestDonnantDonnantDur(TestStrategies):

    def test_round0(self):
        self._test_strategy('donnant_donnant_dur', C)

    def test_round1(self):
        self._test_strategy('donnant_donnant_dur', C, [C], [C])
        self._test_strategy('donnant_donnant_dur', B, [C], [B])

    def test_round2(self):
        self._test_strategy('donnant_donnant_dur', B, [C, C], [C, B])
        self._test_strategy('donnant_donnant_dur', B, [C, B], [B, C])

    def test_round3(self):
        self._test_strategy('donnant_donnant_dur', B, [C, B, B], [B, B, C])
        self._test_strategy('donnant_donnant_dur', C, [C, B, B], [B, C, C])

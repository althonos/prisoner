#ifndef __STRATEGIES_H__
#define __STRATEGIES_H__

#include "choices.h"

typedef int (*strategy_t)(Choices*, Choices*);

/** Always cooperate
 */
int gentille(Choices* self, Choices* other);

/** Always betray
 */
int mechante(Choices* self, Choices* other);

/** Cooperate on the first match, then play what the other played last
 */
int donnant_donnant(Choices* self, Choices* other);

/** Always cooperate until the other betrays, then always betray.
 */
int rancuniere(Choices* self, Choices* other);

/** Play betray, betray, cooperate, betray, betray, cooperate...
 */
int periodique_mechante(Choices* self, Choices* other);

/** Play cooperate, cooperate, betray, cooperate, cooperate, betray....
 */
int periodique_gentille(Choices* self, Choices* other);

/** Play what the other played most, if both choices are even the cooperate
 */
int majorite_mou(Choices* self, Choices* other);

/** Betray on the first match, then play what the other played last
 */
int mefiante(Choices* self, Choices* other);

/** Play what the other played most, if both choices are even the betray
 */
int majorite_dur(Choices* self, Choices* other);

/** On the first 3 matches play betray, cooperate, cooperate.
 *  If the on matches 2 and 3 the other cooperated, then always betray.
 *  Else use the DONNANT-DONNANT strategy.
 */
int sondeur(Choices* self, Choices* other);

/** Cooperate except if the other betrayed in the last two matches.
 */
int donnant_donnant_dur(Choices* self, Choices* other);

/** List of all available strategies
 */
static const strategy_t STRATEGIES[] = {
    gentille,
    mechante,
    donnant_donnant,
    rancuniere,
    periodique_mechante,
    periodique_gentille,
    majorite_mou,
    mefiante,
    majorite_dur,
    sondeur,
    donnant_donnant_dur,
    NULL,
};

#endif

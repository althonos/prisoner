#ifndef __TABLE_H__
#define __TABLE_H__

#include <stdlib.h>
#include <stdio.h>

/** Get the minimal cell size that can contain `max_value` properly.
 */
size_t cell_size(size_t max_value);

void print_table_headers(const char** values, size_t cell_size);
void print_table_row(const char** values, size_t cell_size);
void print_table_bottom(size_t value_count, size_t cell_size);

void print_csv_row(const char** values);
void write_csv_row(const char** values, FILE* out);

#endif

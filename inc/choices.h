#ifndef __CHOICES_H__
#define __CHOICES_H__

#define UNKNOWN -1
#define COOPERATE 1
#define BETRAY 0

#include <stdlib.h>

typedef struct {
	// Last two choices
	int last: 2;
	int prev: 2;
	// Second and third choices
	int second: 2;
	int third: 2;
	// Number of cooperations
	size_t cooperations;
	// Number of betrayals
	size_t betrayals;
} Choices;

/** Create a new choice collection.
 */
Choices* choices_new();

/** Add a choice to a choice collection.
 */
void choices_add(Choices* choices, int choice);

/** Return the number of choices in the collection.
 */
size_t choices_length(Choices* choices);

#endif

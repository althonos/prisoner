#ifndef __CONFRONTATION_H__
#define __CONFRONTATION_H__

#include <stdlib.h>
#include "strategies.h"

typedef struct {
  size_t d;
  size_t c;
  size_t p;
  size_t t;
} weights_t;

/** Create a new collection of weights with the given weights.
 */
weights_t weights_new(size_t d, size_t c, size_t p, size_t t);

/** Return the results of a confrontation between two players.
 */
size_t* confrontation(strategy_t, strategy_t, size_t, weights_t*);

/** Return the result of a confrontation between two players.
 *
 *  Only returns the points gained by the first individual.
 */
size_t confrontation1(strategy_t, strategy_t, size_t, weights_t*);

/** Return the cumulated gains of a strategy agains all of the others
 *
 *  Does not include points earned when playing against itself.
 */
size_t cumul_gains(strategy_t, size_t, weights_t*);

#endif

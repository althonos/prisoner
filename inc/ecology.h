#ifndef __ECOLOGY_H__
#define __ECOLOGY_H__

#include <stdlib.h>
#include <stdbool.h>

#include "confrontation.h"

typedef struct {
	size_t* points;
	size_t* population;
	size_t generation;
	weights_t* weights;
	unsigned disabled;
} Simulation;


/** Create a new simulation with all strategies enabled.
 */
Simulation* simulation_new(size_t initial_pop, weights_t* weights);

/** Create a new simulation with some strategies disabled.
 *
 *  A strategy `i` is disabled by the bitfield `x` if `x & (1 << i)` is not null.
 */
Simulation* simulation_new_disabled(size_t, weights_t*, unsigned);


/** Delete a simulation (and deallocate memory)
 */
void simulation_delete(Simulation* simulation);

/** Advance a simulation to the next generation.
 */
void simulation_iter(Simulation*);

/** Recompute the simulation points.
 *
 *  Useful when the population of the simulation was changed externally.
 */
void simulation_reset_points(Simulation*);

/** Count the total number of points in the simulation.
 */
size_t simulation_total_points(Simulation*);

/** Count the total number of individuals in the simulation.
 */
size_t simulation_total_population(Simulation*);

/** Check whether a strategy is disabled in the simulation.
 */
bool simulation_is_disabled(Simulation*, unsigned);

#endif

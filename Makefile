# Compiler and Linker
CC        := gcc

# Directories
SRCDIR    := src
LIBDIR    := lib
INCDIR    := inc
BUILDDIR  := build
BINDIR    := bin

# Flags
ARFLAGS   := rcs
CFLAGS    += -W -Wall -fPIC
LIBS      += -lm
INC       += -I$(INCDIR)

# Library
LIBNAME   := prisoner
LIBFILES  := $(wildcard $(LIBDIR)/*.c)
LIBOBJS   := $(patsubst $(LIBDIR)/%,$(BUILDDIR)/%,$(LIBFILES:.c=.o))

.PHONY: all
all: lib$(LIBNAME) dilemne1 dilemne2 gulag/client gulag/server


#---------
# LIBRARY
#---------

$(BUILDDIR)/%.o: $(LIBDIR)/%.c $(INCDIR)/%.h
	@mkdir -p $(shell dirname $@)
	$(CC) $(CFLAGS) -c -o $@ $< $(LIBS) $(INC)

# Static library
$(BUILDDIR)/lib$(LIBNAME).a: $(LIBOBJS)
	@mkdir -p $(shell dirname $@)
	$(AR) $(ARFLAGS) $@ $(LIBOBJS)

# Shared library
$(BUILDDIR)/lib$(LIBNAME).so: $(LIBOBJS)
	@mkdir -p $(shell dirname $@)
	$(CC) -shared -Wl,-soname,lib$(LIBNAME).so $^ -o $@

lib$(LIBNAME): $(BUILDDIR)/lib$(LIBNAME).so $(BUILDDIR)/lib$(LIBNAME).a

#----------
# BINARIES
#----------

# Default parameter values
D         	?= 0
T         	?= 5
C         	?= 3
P         	?= 1

# Additional libraries and flags
BIN_LIBS    := -lpthread $(LIBS)
BIN_STATIC  := $(BUILDDIR)/lib$(LIBNAME).a
BIN_CFLAGS  := $(CFLAGS) -DD=$(D) -DT=$(T) -DC=$(C) -DP=$(P)

$(BINDIR)/%: $(SRCDIR)/%.c $(BUILDDIR)/lib$(LIBNAME).a $(SRCDIR)/dilemne1.c
	@mkdir -p $(shell dirname $@)
	$(CC) $(BIN_CFLAGS) -o $@ $< $(BIN_STATIC) $(INC) $(BIN_LIBS)

dilemne1: $(BINDIR)/dilemne1
dilemne2: $(BINDIR)/dilemne2
gulag/server: $(BINDIR)/gulag/server
gulag/client: $(BINDIR)/gulag/client

#---------
# UTILS
#---------

.PHONY: clean
clean:
	@rm -rfv $(BUILDDIR)
	@rm -rfv $(BINDIR)

.PHONY: test
test:
	@python3 -m green -vvkq

### Martin Larralde

# Architecture et Systèmes: Dilemne du Prisonnier

### Organisation du projet

Les fonctions démandées dans l'énoncé ont été implémentées dans une bibliothèque
(`libprisoner`) tandis que les différents programmes ont été implementés chacuns
dans des fichiers différents.

Le répertoire du projet suit l'organisation suivante:

* `bin`: un dossier temporaire où sont stockés les binaires compilés
* `build`: un dossier temporaire où sont stockés les artefacts de compilation
* `inc`: les entêtes de `libprisoner`
* `lib`: les sources de `libprisoner`
* `src`: les sources des différents programmes à réaliser (`dilemne1`, etc.)
* `scripts`: des scripts Python permettant le déploiement des binaires de
  simulation (`scripts/gulag.py`), ou la génération de graphiques
  (`scripts/graphpop.py`)
* `tests`: des tests unitaires de `libprisoner` implementés en Python.

Les scripts Python demandés par l'énoncé nécessitent des modules externes,
mais `gulag.py` dispose d'une version embarquée de `sh`. Les scripts Python
non requis par l'énoncé nécessitent des modules externes: ils peuvent
être installés à l'aide de `pip`, disponible avec les versions récentes de
Python et `setuptools`:

```console
pip install --user -r tests/requirements.txt -r scripts/requirements.txt
```

Le projet a été testé sur les machines de la salle C411 (`glibc+gcc`) ainsi
que dans une image docker Alpine Linux par le service d'intégration continue
de GitLab (`musl+gcc`).

Les programmes sont compilés en *statique* de manière à ne pas avoir à installer
la librairie ou à modifier `LD_LIBRARY_PATH` à l'exécution. Cependant,
`libprisoner.so` est utilisée par les tests afin de ne pas recompiler
la librairie entière à chaque (petit) programme de test.


## Partie 1: Premiers essais

### Implémentation

#### Liste des choix

  Les individus suivant certaines stratégies ont besoin de connaître ou bien
  les choix qu'ils ont effectués aux tours précédents, ou bien ceux qu'ont
  effectués leurs adversaires. Pour stocker ces choix, j'avais dans un premier
  temps implémenté une liste liée, mais après une analyse plus fine on peut
  remarquer qu'il n'y a pas besoin de stocker tous les choix, mais seulement:

  * les choix $2$ et $3$
  * les choices $n-1$ et $n$
  * le nombre de coopérations
  * le nombre de trahisons

  Le fichier `inc/choices.h` contient une définition de la collection `Choices`,
  avec les fonctions suivantes implementées dans `lib/choices.c`:

  * `choices_new()` pour initialiser une nouvelle instance du type `Choices`
  * `choices_add(choices_t, int)` pour ajouter un choix à la collection
  * `choices_length(choices_t)` pour calculer le nombre de choix total de
    la collection

  Toutes les opérations s'effectuent en temps et en mémoire constantes.


#### Stratégies

  Le fichier `inc/strategies.h` définit le type `strategy_t` comme
  `int (*)(choices_t*, choices_t*)`, c'est à dire une fonction prenant en
  entrée deux collections de choix (ceux de l'individu, et ceux de l'opposant),
  et renvoyant un `int` (qui représente un choix: `COOPERATE` ou `BETRAY`).

  Etant donnée l'implementation de la liste des choix, le calcul de toutes les
  stratégies est effectué en temps constant et en mémoire constante.

  Les stratégies sont implémentées dans le fichier `lib/strategies.c`, et
  sont testées avec des tests unitaires pour entre $2$ et $5$ rounds chacunes
  dans le fichier `tests/test_strategies.py`.


#### Confrontation

  Le fichier `lib/confrontation.h` définit le type `weights_t` permettant de
  garder en mémoire les gains possibles pendant une confrontation ($D, C, P, T$).

  Le fichier `lib/confrontation.c` contient deux fonctions différentes pour
  calculer le résultat d'une confrontation itérée entre deux stratégies:

  * `size_t* confrontation(strategy_t, strategy_t, size_t, weights_t)` prenant en
    entrée les deux stratégies à faire s'affronter, le nombre d'itération à simuler,
    et la valeur des gains. Cette fonction renvoie une réference vers un tableau
    dont le premier élément est le gain de l'individu avec la première stratégie,
    et le second le gain de l'individu avec la deuxième stratégie.
  * `size_t confrontation1(strategy_t, strategy_t, size_t, weights_t)` calcule la
    même chose, mais ne renvoie que les gains du premier individu. Il n'y a donc
    pas besoin de libérer de mémoire avec `free`, contrairement à
    un appel de `confrontation`.



### Résultats

#### Génération de tables

  `libprisoner` implémente des fonctions pour la génération de tables ASCII
  dans le fichier `lib/table.c`:

  * `print_table_headers(const char** headers, size_t cell_size)` affiche les entêtes
    du tableau, dans des cases de taille `cell_size`.
  * `print_table_row(const char** values, size_t cell_size)` affiche une ligne
    contenant les valeurs, dans des cases de taille `cell_size`
  * `print_table_bottom(size_t value_count, size_t cell_size)` affiche la dernière
    ligne.

  Les deux premières fonctions prennent en entrée un tableau de chaînes de caractères
  `NULL`-terminated, c'est à dire dont le dernier élément est `NULL`. La dernière
  fonction prend seulement le nombre de valeurs dans une ligne.

  Il est également possible de générer des tables **CSV** à l'aide de la
  fonction `print_csv_row(const char** values)` prenant également un tableau
  `NULL`-terminated.


#### `bin/dilemne1`

  L'exécutable `dilemne1` permet de générer une table des confrontations et une
  table des cumuls des points pour un nombre de coups donné en paramètres.

  Le `Makefile` définit une cible `dilemne1`:

  ```console
  make dilemne1
  ```

  La valeur des paramètres $D, C, P, T$ peut être modifiée lors de la compilation
  en passant des valeurs différentes à `make`:

  ```console
  make clean dilemne1 D=1 C=10 P=3 T=2
  ```

  Le programme affiche une aide sommaire quand on lui passe le paramètre `--help`
  en deuxième argument. Il peut sinon être invoqué directement avec le nombre
  de coups demandé:

  ```console
  bin/dilemne1 1000
  ```

  Le résultat produit deux tables (exemple ici pour $1000$ coups):

  ![](table1.png)

  ![](table2.png)


## Partie 2: Simulation écologique

### Implémentation

Les données nécessaires à la simulation écologique telle que demandée dans
l'énoncé sont stockées dans une structure `Simulation`, définie dans
`inc/ecology.h`, gardant en mémoire:

* la génération où se trouve la simulation: `Simulation->generation`
* la population par stratégie à cette génération `Simulation->population`
* les points gagnés par chaque stratégie à cette génération: `Simulation->points`
* la valeur des paramètres $D, C, P, T$ pour cette simulation: `Simulation->weights`
* les stratégies désactivées dans la simulation `Simulation->disabled` (sous
  forme de *bitfield*)

Les opérations implémentées sont:

* la création d'une nouvelle simulation à partir d'une population initiale par
  groupe, et des valeurs des (`simulation_new(size_t, weights_t*)`)
* le passage à la génération suivante (`simulation_iter(Simulation*)`)
* la déallocation d'un simulation (`simulation_delete(Simulation*)`)
* le calcul du total des points (`simulation_total_points(Simulation*)`)
* le calcul de la population totale (`simulation_total_population(Simulation*)`)

Etant donné que la formule de calcul de la population utilise des nombres
réels, on doit nécessairement procéder à des approximations. Afin de maintenir
une population **constante**, on calcule la taille de la nouvelle population
en procédant à un arrondi (avec la fonction `roundl`), et si jamais de nouveaux
individus sont *apparus*, on les retire du groupe ayant la population la plus
élevée.

### Résultats

#### `bin/dilemne2`

L'exécutable `dilemne2` permet de générer une table de la population par
stratégie en fonction du temps pour un nombre de générations et une population
initiale par groupe donnée en argument.

Tout comme `dilemne1`, le `Makefile` définit une cible `dilemne2`, et les
paramètres $D, C, P, T$ peuvent être modifiés lors de la compilation:
```console
make dilemne2 D=3
```

Le programme affiche une aide sommaire quand on lui passe l'argument `--help`,
et prend en argument la population initiale et le nombre de générations
à simuler. Pour simuler 30 générations et une population de départ de 100
individus par stratégies, on appelle le programme avec les paramètres suivants:
```console
bin/dilemne2 100 30
```

Le paramètre `--csv` permet de générer une table **CSV** au lieu d'un tableau
ASCII, afin de permettre à un autre programme de lire le résultat et de générer
un graphique.

#### Graphique

Le graphique ci-dessous montre l'évolution d'un groupe de $1000$ individus
par stratégie sur $30$ générations:

![](graphpop.png)

Il a été généré en utilisant le script `graphpop.py`:
```console
scripts/graphpop.py -i 100 -g 30
```

Le script `graphpop.py` utilise les modules externes `fs` et `sh` pour la gestion
du système de fichier et des appels systèmes, `matplotlib` pour la génération du
graphe, et `pandas` pour la lecture et la manipulation de la table **CSV**
générée par `dilemne2`.



## Partie 3: Simulation distribuée

### Architecture

Pour implémenter la simulation spécifiée dans l'énoncé, j'ai choisir de suivre
un architecture client-serveur. Le client utilise seulement le thread principal
pour exécuter la simulation, tandis que le serveur fonctionne avec plusieurs threads:

* le thread principal, qui crée un nouveau thread à chaque connexion
* un thread par client qui exécute la fonction `supervise`
* un thread qui se charge de rapporter la progression de la simulation
* un thread qui se charge de gérer les migrations entre les clients

![](arch.png)

Pour communiquer, le serveur et client utilisent un protocole défini dans
`common.h`:

* **REGISTER id hostname** (C $\rightarrow$ S): le client envoie au serveur
  son identifiant et son *hostname* (premier message après la connexion).
* **GENERATION n** (C $\rightarrow$ S): le client envoie la génération auquel
  se trouve sa simulation sous forme de nombre non-signé
* **POP pop** (C $\rightarrow$ S): le client envoie l'état de sa population
  sous forme d'une ligne CSV de 11 valeurs numériques
* **POINTS points** (C $\rightarrow$ S): le client envoie les points gagnés à
  la génération actuelle sous forme d'une ligne CSV de 11 valeurs numériques
* **CLOSE** (C $\rightarrow$ S): le client informe le serveur qu'il ferme la
  connexion, le serveur peut alors arrêter le thread dédié
* **DONE** (C $\leftrightarrow$ S): informe que l'envoi de donnnées est terminé
* **DISABLED d** (C $\rightarrow$ S): informe le serveur de quelles stratégies sont
  interdites par le client
* **MIGRATE id n** (C $\leftrightarrow$ S): informe d'un influx de population
  pour la stratégie donnée


### Invocation des programmes

#### Serveur

Le serveur se lance en prenant en argument le port sur lequel écouter des
connexions (par exemple, sur le port 6666):
```
bin/gulag/server 6666
```

Le client dispose d'une interface plus complète, implémentée avec `argp.h`.
Les arguments nécessaires sont le nom de domaine du serveur et le port sur
lequel il écoute (par exemple, pour un serveur sur `localhost` qui écouté
sur le port 6666):
```
bin/gulag/client localhost 6666
```

La liste complète des options peut être obtenue avec l'argument `--help`:
```
bin/gulag/client --help
```

Par exemple, pour créer un client:

* où les stratégies `gentille` et `mechante` sont désactivées
* dont la population initiale par groupe est de 500 individus
* où $T = 20$ et $C = 10$
* dont l'identifiant est `Test`
* se connectant à un serveur sur `01.dptinfo` écoutant sur le port `8000`

on invoquerait le programme avec les paramètres suivants:
```
bin/gulag/client 01.dptinfo 8000 -C10 -T20 --initial-pop=500 --disable=0 --disable=1
```

**NB:** les stratégies sont numérotées en partant de 0 dans l'ordre de la
première page de l'énoncé, et dans l'ordre dans lequel le serveur les affiches
dans le tableau.

Les programmes s'arrêtent en envoyant un signal d'interruption (SIGINT), i.e.
Ctrl+C dans le terminal.


### Interface

Le serveur dispose d'un *pseudo-GUI* pour suivre le déroulement des simulations,
c'est à dire d'un tableau mis à jour en temps réél en fonction des progrès des
différentes simulation et de la connexion / déconnexion d'un client.

![](ui.png)

Cependant, il peut être intéressant d'obtenir les valeurs numériques dans un
format utilisable par la machine; dans ce cas, quand le serveur détecte que
la sortie standard n'est pas un terminal, alors il produit un journal au
format **CSV**, où chaque ligne contient:

* l'identifiant du client
* ce à quoi correspondent les valeurs: points ou population
* la génération à laquelle se trouve le client
* les valeurs pour les 11 stratégies

Ce format permet de facilement récupérer les valeurs dans le terminal
avec `grep`: par exemple, pour n'avoir que les populations:
```
bin/gulag/server localhost 6666 | grep population
```
ou seulement les valeurs pour le client `Guantanamo`
```
bin/gulag/server localhost 6666 | grep Guantanamo
```

### Migration

* Client: pour décider s'il doit y a voir migration, on effectue un test de
  Bernouilli pour chaque individu de chaque groupe (avec une proba assez faible,
  je l'aurais rendue variable si j'avais eu le temps, par exemple en fonction
  de la proportion d'un groupe dans la population globale)
* Serveur: le serveur reçoit les émigrants de chaque client, puis les transfère
  stratégie par stratégie de manière aléatoire à un client qui autorise la
  stratégie en question (il est possible qu'ils soient renvoyés au client source !).

### Déploiement

Nécessite:

* un accès via SSH sans mot-de-passe vers toutes les machines
* que les clients et le serveur soient dans le même réseau local

Une machine peut avoir plusieurs clients, et le serveur n'a pas besoin d'être
sur une machine à part.

Il est possible que le programme plante si le port sur lequel le serveur doit
écouter n'est pas disponible: dans ce cas, il faut en changer manuellement dans
le fichier de configuration.

On donne en argument au script `scripts/gulag.py` un fichier de configuration
dans un format proche du `INI`. Le fichier contient obligatoirement une section
**Hypervisor**, avec les clefs `hostname` et `port`.

Il y a ensuite une section par client: l'identifiant de la section sera
l'identifiant du client, et on peut ensuite modifier:

* le nom de domaine de la machine sur laquelle déployer le client (`hostname`)
* les paramètres $D, C, P, T$
* la population initiale par groupe (`initial-pop`)
* la liste des stratégies à désactiver (`disable`)

cf. `scripts/gulag-localhost.cfg` pour un exemple de fichier de configuration
qui déploie 5 clients sur la machine locale avec des paramètres variés.


### Miscellanées

Le nombre de clients pouvant se connecter au même moment au serveur est limité
par le nombre de threads pouvant être lancés simultanément par `pthreads`.

Les binaires `gulag/client` et `gulag/server` ont été analysés avec **Valgrind**
et n'ont à priori pas de fuite de mémoire.

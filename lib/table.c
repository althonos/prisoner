#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "table.h"


size_t cell_size(size_t max_value) {
    return ((size_t) log10(max_value)) + 3;
}


void print_table_headers(const char** headers, size_t cell_size) {
    for (unsigned i = 0; headers[i] != NULL; i++) {
        printf(i == 0 ? "┌" : "┬");
        for (unsigned j = 0; j < cell_size; j++) printf("─");
    }; printf("┐\n");
    for (unsigned i = 0; headers[i] != NULL; i++) {
        printf("│ %*s ", (int) cell_size-2, headers[i]);
    }; printf("│\n");
    for (unsigned i = 0; headers[i] != NULL; i++) {
        printf(i == 0 ? "├" : "┼");
        for (unsigned j = 0; j < cell_size; j++) printf("─");
    }; printf("┤\n");
}


void print_table_row(const char** values, size_t cell_size) {
    for (unsigned i = 0; values[i] != NULL; i++) {
        printf("│ %*s ", (int) cell_size-2, values[i]);
    }; printf("│\n");
}


void print_table_bottom(size_t value_count, size_t cell_size) {
  for (unsigned i = 0; i < value_count; i++) {
      printf(i == 0 ? "└" : "┴");
      for (unsigned j = 0; j < cell_size; j++) printf("─");
  }; printf("┘\n");
}



void write_csv_row(const char** values, FILE* out) {
    if (values[0] != NULL) {
      fprintf(out, "%s", values[0]);
      for (unsigned i = 1; values[i] != NULL; i++) fprintf(out, ",%s", values[i]);
      fprintf(out, "\n");
    }
}

void print_csv_row(const char** values) {
    write_csv_row(values, stdout);
}

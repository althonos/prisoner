#include <stdlib.h>
#include <stdbool.h>
#include <math.h>

#include "confrontation.h"
#include "ecology.h"


size_t _id_confrontation(int i, int j, int rounds, weights_t* weights) {
	return confrontation1(
		STRATEGIES[i],
		STRATEGIES[j],
		rounds,
		weights
	);
}

void _iter_points(Simulation* sim, size_t generation) {

	size_t* points = sim->points;
	size_t* pop = sim->population;
	weights_t* w = sim->weights;

	for (int s = 0; s < 11; s++) {
		points[s] = pop[s] ? 0 : _id_confrontation(s, s, generation+1, w) * (pop[s] - 1);
		for (int t = 0; t < 11; t++)
			if (t != s)
				points[s] += _id_confrontation(s, t, generation+1, w) * pop[t];
		points[s] *= pop[s];
	}
}

void _iter_pop(Simulation* sim) {

	long double total_points = (long double) simulation_total_points(sim);

	size_t total_pop = simulation_total_population(sim);
	size_t* points = sim->points;
	size_t* pop = sim->population;

	for (unsigned i = 0; i < 11; i++)
		if (pop[i] != 0)
			pop[i] = (size_t) roundl(total_pop * points[i] / total_points);

	size_t new_pop = simulation_total_population(sim);

	// Make sure the total population size stays constant:
	// to do so, we either remove any additional individual from the most
	// populated group, or we add missing individuals to the smallest non-empty
	// group.
	if (new_pop < total_pop) {
			unsigned argmin = 0;
			for (unsigned i = 0; i < 11; i++)
			    if ((pop[argmin] == 0) || ((pop[i] < pop[argmin]) && (pop[i] != 0)))
							argmin = i;
			pop[argmin] += total_pop - new_pop;
	} else if (new_pop > total_pop) {
			unsigned argmax = 0;
			for (unsigned i = 0; i < 11; i++)
					if ((pop[argmax] == 0) || ((pop[i] > pop[argmax]) && (pop[i] != 0)))
							argmax = i;
			pop[argmax] -= new_pop - total_pop;
	}

}


Simulation* simulation_new(size_t initial_pop, weights_t* weights) {
		return simulation_new_disabled(initial_pop, weights, 0);
}

Simulation* simulation_new_disabled(size_t initial_pop, weights_t* weights, unsigned disabled) {
		Simulation* simulation = (Simulation*) malloc(sizeof(Simulation));
		simulation->points = calloc(11, sizeof(size_t));
		simulation->population = malloc(11*sizeof(size_t));
		simulation->generation = 0;
		simulation->weights = weights;
		simulation->disabled = disabled;
		for (unsigned s = 0; s < 11; s++)
				simulation->population[s] = simulation_is_disabled(simulation, s) ? 0 : initial_pop;
		_iter_points(simulation, 0);
		return simulation;
}


void simulation_delete(Simulation* simulation) {
	free(simulation->points);
	free(simulation->population);
	free(simulation);
}

size_t simulation_total_population(Simulation* simulation) {
	size_t total = 0;
	for (int i = 0; i < 11; i++)
		total += simulation->population[i];
	return total;
}

size_t simulation_total_points(Simulation* simulation) {
	size_t total = 0;
	for (int i = 0; i < 11; i++)
		total += simulation->points[i];
	return total;
}

void simulation_iter(Simulation* simulation) {
	_iter_pop(simulation);
	_iter_points(simulation, ++(simulation->generation));
}

void simulation_reset_points(Simulation* simulation) {
	_iter_points(simulation, simulation->generation);
}

inline bool simulation_is_disabled(Simulation* simulation, unsigned strategy_id) {
		return simulation->disabled & (1 << strategy_id);
}

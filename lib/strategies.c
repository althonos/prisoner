#include <stdlib.h>
#include <stdio.h>
#include <errno.h>

// Certaines stratégies n'utilisent pas les listes de choix
// passées en arguments; mais pour avoir toujours la même
// signature, on leur donne quand même deux arguments.
// Pour que GCC se taise, on désactive les warnings le temps
// de compiler l'implémentation des stratégies.
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"

#include "choices.h"
#include "strategies.h"


// Je coopère toujours
int gentille(Choices* self, Choices* other) {
		return COOPERATE;
}

// Je trahis toujours
int mechante(Choices* self, Choices* other) {
		return BETRAY;
}

// Je coopère à la 1ere partie puis je joue ce qu'à joué l'autre
// à la partie précédente
int donnant_donnant(Choices* self, Choices* other) {
		return (self->last == UNKNOWN) ? COOPERATE : other->last;
}

// Je coopère mais dès que mon adversaire me trahit, je trahis toujours
int rancuniere(Choices* self, Choices* other) {
		return (other->betrayals > 0) ? BETRAY : COOPERATE;
}

// Je joue cooperer, cooperer, trahir, cooperer, cooperer, trahir...
int periodique_gentille(Choices* self, Choices* other) {
		if (self->second == UNKNOWN) return COOPERATE;
		return (self->last == COOPERATE && self->prev == COOPERATE) ? BETRAY : COOPERATE;
}

// Je joue trahir, trahir, cooperer, trahir, trahir, cooperer...
int periodique_mechante(Choices* self, Choices* other) {
		if (self->second == UNKNOWN) return BETRAY;
		return (self->last == BETRAY && self->prev == BETRAY) ? COOPERATE : BETRAY;
}

// Je joue ce que l'adversaire a joué en majorité, en cas d'égalité
// ou à la première partie je coopère
int majorite_mou(Choices* self, Choices* other) {
		return (other->cooperations >= other->betrayals) ? COOPERATE : BETRAY;
}

// Je trahis à la première partie puis je joue ce qu'à joué l'autre
// à la partie précédente
int mefiante(Choices* self, Choices* other) {
		return (other->last == UNKNOWN) ? BETRAY : other->last;
}

// Je joue ce que l'adversaire a joué en majorité, en cas d'égalité
// ou à la première partie je trahis
int majorite_dur(Choices* self, Choices* other) {
        return (other->betrayals >= other->cooperations) ? BETRAY : COOPERATE;
}

// Aux trois premières parties je joue trahir, coopérer, coopérer
// Si aux parties 2 et 3 l'adversaire a coopéré, je trahis toujours,
// Sinon j'opte pour la stratégie DONNANT-DONNANT
int sondeur(Choices* self, Choices* other) {
		if (self->third == UNKNOWN) return (self->last == UNKNOWN) ? BETRAY : COOPERATE;
		if (other->second == COOPERATE && other->third == COOPERATE) return BETRAY;
		return donnant_donnant(self, other);
}

// Je coopère sauf si mon adversaire trahit lors de l'une des
// deux parties précédentes
int donnant_donnant_dur(Choices* self, Choices* other) {
		if ((other->last != BETRAY) && (other->prev != BETRAY)) return COOPERATE;
		return BETRAY;
}

#pragma GCC diagnostic pop

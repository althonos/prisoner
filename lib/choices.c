#include <stdlib.h>

#include "choices.h"
#include "confrontation.h"

Choices* choices_new() {
		Choices* choices = malloc(sizeof(Choices));
		choices->second = choices->third = UNKNOWN;
		choices->last = choices->prev = UNKNOWN;
		choices->cooperations = choices->betrayals = 0;
		return choices;
}

void choices_add(Choices* choices, int choice) {

		if (choices->last != UNKNOWN) {
				if (choices->second == UNKNOWN) choices->second = choice;
				else if (choices->third == UNKNOWN) choices->third = choice;
		}

		choices->prev = choices->last;
		choices->last = choice;

		if (choice == COOPERATE) choices->cooperations++;
		else choices->betrayals++;
}

inline size_t choices_length(Choices* choices) {
		return choices->cooperations + choices->betrayals;
}

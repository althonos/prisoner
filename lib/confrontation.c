#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>

#include "choices.h"
#include "strategies.h"
#include "confrontation.h"

int _gain(int choice_self, int choice_other, weights_t* weights) {
	if (choice_self == COOPERATE) {
		if (choice_other == COOPERATE) return weights->c;
		else return weights->d;
	} else {
		if (choice_other == COOPERATE) return weights->t;
		else return weights->p;
	}
}

weights_t weights_new(size_t d, size_t c, size_t p, size_t t) {
		return (weights_t){d, c, p, t};
}

size_t* confrontation(strategy_t strat1, strategy_t strat2, size_t rounds, weights_t* weights) {

	int choice1, choice2;

	Choices* choices1 = choices_new();
	Choices* choices2 = choices_new();

	size_t* gains = (size_t*) calloc(2, sizeof(size_t));

	for (size_t i = 0; i < rounds; i++) {
		choice1 = strat1(choices1, choices2);
		choice2 = strat2(choices2, choices1);
		choices_add(choices1, choice1);
		choices_add(choices2, choice2);
		gains[0] += _gain(choice1, choice2, weights);
		gains[1] += _gain(choice2, choice1, weights);
	}

	free(choices1);
	free(choices2);

	return gains;
}

size_t confrontation1(strategy_t strat1, strategy_t strat2, size_t rounds, weights_t* weights) {
	size_t* gains = confrontation(strat1, strat2, rounds, weights);
	size_t gain = gains[0];
	free(gains);
	return gain;
}

size_t cumul_gains(strategy_t strategy, size_t rounds, weights_t* weights) {
	size_t total = 0;
	for (int i = 0; i < 11; i++)
		if (STRATEGIES[i] != strategy)
			total += confrontation1(strategy, STRATEGIES[i], rounds, weights);
	return total;
}

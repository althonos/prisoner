#!/usr/bin/env python3
# coding: utf-8
from __future__ import absolute_import
from __future__ import unicode_literals

import os
import sys
import time
import json
import signal
import atexit
import hashlib
import argparse
import functools
import threading
import subprocess
import configparser
import urllib.request

from functools import reduce

# Local copy of sh, so that it's not needed to install it
from lib import sh


prog = argparse.ArgumentParser(
    description="deploy a gulag network from a configuration file")
prog.add_argument(
    "cfg", metavar="CONFIG", action="store",
    help="a configuration file")


if __name__ == "__main__":

    # Parse the arguments and read the configuration file
    args = prog.parse_args()
    config = configparser.ConfigParser()
    with open(args.cfg) as config_file:
        config.read_file(config_file)

    # Compile the required binaries
    projdir = os.path.abspath(os.path.join(__file__, "..", ".."))
    sh.make("gulag/client", "gulag/server", directory=projdir)
    bin_client = os.path.join(projdir, "bin", "gulag", "client")
    bin_server = os.path.join(projdir, "bin", "gulag", "server")

    # Hypervisor
    hypervisor_host = config.get("Hypervisor", "hostname")
    hypervisor_port = config.getint("Hypervisor", "port")

    # Clients
    client_hosts = []
    for section in filter("Hypervisor".__ne__, config.sections()):
        host = config.get(section, "hostname")
        if config.has_option(section, "ssh-user"):
            host = "@".join([config.get(section, "ssh-user"), host])
        client_hosts.append(host)

    try:
        # Deploy client binaries
        for idx, host in enumerate(client_hosts):
            sh.scp(bin_client, "{}:{}".format(host, "gulag-{}".format(idx)))

        # Deploy server binary
        sh.scp(bin_server, "{}:{}".format(hypervisor_host, "gulag-server"))

        # Launch clients
        clients = []
        for idx, section in enumerate(filter("Hypervisor".__ne__, config.sections())):
            disabled = json.loads(config.get(section, "disable", fallback="[]"))
            clients.append(sh.ssh(
                client_hosts[idx],
                "./gulag-{}".format(idx),
                "--id='{}'".format(section),
                "--initial-pop='{}'".format(config.get(section, 'initial-pop')),
                "-D{}".format(config.get(section, "D")),
                "-C{}".format(config.get(section, "C")),
                "-T{}".format(config.get(section, "T")),
                "-P{}".format(config.get(section, "P")),
                functools.reduce("{}-d{} ".format, disabled, ""),
                hypervisor_host,
                str(hypervisor_port),
                # remove binary when finished
                "; rm gulag-{}".format(idx),
                _bg=True,
            ))

        # Clear screen
        if os.isatty(sys.stdout.fileno()):
            sh.clear(_out=sys.stdout)

        # Launch server
        hypervisor = sh.ssh(
            hypervisor_host,
            "./gulag-server",
            str(hypervisor_port),
            "--force-ui" if os.isatty(sys.stdout.fileno()) else "",
            # Remove the binary when execution is done
            "; rm gulag-server",
            _bg=True,
            _in=sys.stdin,
            _out=sys.stdout,
        )

        # Wait for the user to exit through Ctrl+C
        while True:
            time.sleep(1)

    # On Ctrl+C
    except KeyboardInterrupt:

        # Avoid recatching a Ctrl+C (if the user is mashing the keyboard)
        signal.signal(signal.SIGINT, lambda a,b: None)

        # Close the clients and remove the binary
        for client in clients:
            client.signal(signal.SIGINT)

        # Interrupt the server
        hypervisor.signal(signal.SIGINT)

        # Clear screen
        if os.isatty(sys.stdout.fileno()):
            sh.clear(_out=sys.stdout)

#!/usr/bin/env python3
# coding: utf-8

import io
import os
import argparse

import fs
import sh
import pandas
import matplotlib.pyplot as plt

# Create a parser for the arguments
prog = argparse.ArgumentParser(
    description="generate a graph showing the population evolution")
prog.add_argument(
    "-i", "--initial-population", action="store", type=int, default=100,
    help="initial strategy group size")
prog.add_argument(
    "-g", "--generations", action="store", type=int, default=30,
    help="number of generations to compute")

# Open the project directory as a filesystem
projfs = fs.open_fs(os.path.join(os.path.abspath(__file__), '..', '..'))


if __name__ == "__main__":

    # Parse the arguments
    args = prog.parse_args()

    # Compile 'dilemne2' if needed
    if not projfs.isfile('bin/dilemne2'):
        sh.make('dilemne2', directory=projfs.getsyspath('/'))

    # Call 'dilemne2' with the given arguments and enable CSV output
    dilemne2 = sh.Command(projfs.getsyspath('bin/dilemne2'))
    res = dilemne2(args.initial_population, args.generations, csv=True)

    # Import the CSV table to pandas and plot it
    table = pandas.read_csv(io.BytesIO(res.stdout), index_col=0)
    table.plot()

    # Show the graph
    plt.xlim([0, args.generations])
    plt.xlabel("Générations")
    plt.ylabel("Population")
    plt.grid()
    plt.show()

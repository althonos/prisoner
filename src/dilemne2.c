#include <stdio.h>
#include <string.h>
#include <errno.h>

#include "ecology.h"
#include "table.h"


const char* HEADERS[] = {
	"(t)", "Gent.", "Mech.", "DnDn.", "Ranc.",
	"PeriodM.", "PeriodG.", "MajMou.", "Mef.",
	"MajDur.", "Sond.", "DnDnDur.", NULL
};

int CSV = 0;



void print_sim_pop(Simulation* sim, size_t cell_size) {
    char** row = calloc(13, sizeof(char*));
    for (unsigned i = 0; i < 12; i++) {
        row[i] = calloc(cell_size, sizeof(char));
        sprintf(row[i], "%zu", i == 0 ? sim->generation : sim->population[i-1]);
    }

		if (CSV) print_csv_row((const char**) row);
		else print_table_row((const char**) row, cell_size);

    for (unsigned i = 0; i < 12; i++) free(row[i]);
    free(row);
}


void usage(const char* prog) {
	printf("Usage:\n");
	printf("    %s <initial_pop> <rounds> [--csv]\n", prog);
	printf("Affiche l'évolution des groupes de stratégies de taille\n");
	printf("inital <initial_pop> pendant <rounds> générations.\n\n");
	printf("Arguments:\n");
	printf("    --csv        Génère une table au format CSV\n");
	printf("\n");
	exit(EINVAL);
}


int main(int argc, const char** argv) {

    if ((argc > 4) || (argc < 3) || ((argc == 4) && (strcmp(argv[3], "--csv") != 0)))
				usage(argv[0]);
		if (argc == 4)
				CSV = 1;

		char* endptr; errno = 0;
    long long initial_pop = strtoll(argv[1], &endptr, 10);
  	if ((initial_pop < 0) || errno || (endptr == argv[1])) {
  		printf("Erreur: <initial_pop> doit être un nombre positif ou nul\n");
  		return EINVAL;
  	}

		errno = 0;
    long long rounds = strtoll(argv[2], &endptr, 10);;
		if ((rounds < 0) || errno || (endptr == argv[2])) {
  		printf("Erreur: <rounds> doit être un nombre positif ou nul\n");
  		return EINVAL;
  	}

    size_t size = cell_size(initial_pop * 11);
    if (size < 10) size = 10;

		if (CSV)
				print_csv_row(HEADERS);
		else
				print_table_headers(HEADERS, size);

		weights_t weights = weights_new(D, C, P, T);
    for (Simulation* sim = simulation_new(initial_pop, &weights);
				 sim->generation <= (size_t) rounds;
				 simulation_iter(sim)) print_sim_pop(sim, size);

		if (!CSV)
				print_table_bottom(12, size);

		return 0;
}

#include <stdio.h>
#include <string.h>
#include <errno.h>

#include "confrontation.h"
#include "strategies.h"
#include "choices.h"
#include "table.h"

#ifndef max
		#define max(a,b) ((a) > (b) ? (a) : (b))
#endif


char* HEADERS[] = {
	"", "Gent.", "Mech.", "DnDn.", "Ranc.",
	"PeriodM.", "PeriodG.", "MajMou.", "Mef.",
	"MajDur.", "Sond.", "DnDnDur.", NULL
};


void print_confrontation_table(size_t rounds, weights_t* w) {
		size_t cell, gain;
		char* row_values[13] = {0};

		cell = max(cell_size(rounds * max(w->d, max(w->c, max(w->p, w->t)))), 10);
		print_table_headers((const char**) HEADERS, cell);

		for (unsigned i = 0; i < 11; ++i) {
				row_values[0] = HEADERS[i+1];
				for (unsigned j = 0; j < 11; ++j) {
						gain = confrontation1(STRATEGIES[i], STRATEGIES[j], rounds, w);
						row_values[j+1]  = malloc(sizeof(char)*cell);
						sprintf(row_values[j+1], "%zu", gain);
				}
				print_table_row((const char**) row_values, cell);
				for (unsigned j = 1; j < 12; j++)
						free(row_values[j]);
		}

		print_table_bottom(12, cell);
}


void print_gains_table(size_t rounds, weights_t* w) {
		size_t cell, gain;
		char* row_values[13] = {0};

		cell = max(cell_size(rounds * 10 * max(w->d, max(w->c, max(w->p, w->t)))), 10);
		print_table_headers((const char**) HEADERS + 1, cell);

		for (unsigned j = 0; j < 11; j++) {
				gain = cumul_gains(STRATEGIES[j], rounds, w);
				row_values[j] = malloc(sizeof(char)*cell);
				sprintf(row_values[j], "%zu", gain);
		};
		row_values[11] = NULL;
		print_table_row((const char**) row_values, cell);
		for (unsigned j = 1; j < 12; j++)
				free(row_values[j]);

		print_table_bottom(11, cell);
}


int main(int argc, const char** argv) {


	if (argc != 2 || !strcmp(argv[1], "--help") || !strcmp(argv[1], "-h")) {
			printf("Usage:\n");
			printf("    %s <rounds>\n", argv[0]);
			printf("Affiche la table de confrontation et la table de cumul\n");
			printf("de points pour le nombre de coups passés en paramètre.\n\n");
			return EINVAL;
	}

	char* endptr; errno = 0;
	long long rounds = strtoll(argv[1], &endptr, 10);
	if ((endptr == argv[1]) || errno || (rounds <= 0)) {
      printf("Erreur: <rounds> doit être un nombre positif\n");
			return EINVAL;
	}

	weights_t weights = weights_new(D, C, P, T);

	printf("╔════════════════╗\n");
	printf("║ Confrontations ║\n");
	printf("╚════════════════╝\n");

	print_confrontation_table(rounds, &weights);

	printf("\n");
	printf("╔═════════════════╗\n");
	printf("║ Cumul des gains ║\n");
	printf("╚═════════════════╝\n");

	print_gains_table(rounds, &weights);

}

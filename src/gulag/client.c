#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <unistd.h>
#include <netinet/in.h>
#include <errno.h>
#include <netdb.h>
#include <string.h>
#include <argp.h>
#include <signal.h>
#include <time.h>
#include <limits.h>

#include "ecology.h"
#include "confrontation.h"

#include "common.h"




// ARGUMENT PARSING
// adapted from "https://www.gnu.org/software/libc/manual/html_node/Argp-Example-3.html"

static char doc[] = "City client -- simulate a carceral city";
static char args_doc[] = "HOSTNAME PORT";

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wmissing-field-initializers"

static struct argp_option options[] = {
    { "initial-pop", 'p', "POP",   0, "initial population (groupwise)"},
    { "id",          'i', "ID",    0, "id of the city"},
    { 0,             'D', "D",     0, "points gained when cooperating with a traitor"},
    { 0,             'C', "C",     0, "points gained when cooperating with a loyalist"},
    { 0,             'P', "P",     0, "points gained when betraying a traitor"},
    { 0,             'T', "T",     0, "points gained when betraying a loyalist"},
    {"disable",      'd', "STRAT", 0, "disable a strategy"},
    { 0 }
};

struct arguments {
    char* id;
    char* server;
    size_t port;
    size_t initial_pop;
    int disable: 12;
    weights_t weights;
};

static int parse_size_t(char* arg, size_t* result) {
    char* endptr;
    *result = (size_t) strtoull(arg, &endptr, 10);
    if (*endptr != 0) return EINVAL;
    return 0;
}

static error_t parse_opt(int key, char* arg, struct argp_state *state) {
    struct arguments* args = state->input;
    int error;

    switch (key) {
        case 'd': {
            size_t strat;
            if ((error = parse_size_t(arg, &strat))) return error;
            if (strat < 11) args->disable |= 1 << strat;
            break;
        }
        case 'i':
            args->id = arg;
            break;
        case 'p':
            if ((error = parse_size_t(arg, &args->initial_pop))) return error;
            break;
        case 'C':
            if ((error = parse_size_t(arg, &(args->weights.c)))) return error;
            break;
        case 'D':
            if ((error = parse_size_t(arg, &(args->weights.d)))) return error;
            break;
        case 'T':
            if ((error = parse_size_t(arg, &(args->weights.t)))) return error;
            break;
        case 'P':
            if ((error = parse_size_t(arg, &(args->weights.p)))) return error;
            break;
        case ARGP_KEY_ARG:
            switch (state->arg_num) {
                case 0:
                    args->server = arg;
                    break;
                case 1:
                    if ((error = parse_size_t(arg, &args->port))) return error;
                    break;
                default:
                    argp_usage(state);
            }
            break;
        case ARGP_KEY_END:
            if (state->arg_num < 2) argp_usage(state);
            break;
        default:
            return ARGP_ERR_UNKNOWN;
    }
    return 0;
}

static struct argp argp = { options, parse_opt, args_doc, doc };

#pragma GCC diagnostic pop



// SIMULATION

static bool active = true;

void intHandler() {
    active = false;
}

void simulate(int socket, char* id, int initial_pop, weights_t weights, unsigned disabled) {

    // Open r/w channels from the socket
    FILE* chan_in = fdopen(socket, "r");
    FILE* chan_out = fdopen(socket, "w");

    // Get local hostname
    char hostname[HOST_NAME_MAX+1];
    if (gethostname(hostname, HOST_NAME_MAX)) {
        fprintf(stderr, "could not resolve hostname\n");
        exit(errno);
    }

    // Introduce client to the server
    fprintf(chan_out, "%c %s@%s\n", PROTO_REGISTER, id, hostname);
    fflush(chan_out);

    // Claim which strategies are disabled
    fprintf(chan_out, "%c %u\n", PROTO_DISABLED, disabled);
    fflush(chan_out);

    // Create simulation and disable some groups
    Simulation* sim = simulation_new_disabled(initial_pop, &weights, disabled);

    // Launch simulation loop
    char header, eol, *args;
    while (active) {

        // Reports the points
        fprintf(chan_out, "%c ", PROTO_POINTS);
        for (unsigned i = 0; i < 11; i++)
            fprintf(chan_out, "%zu,", sim->points[i]);
        fprintf(chan_out, "\n");
        fflush(chan_out);

        // Reports the population
        fprintf(chan_out, "%c ", PROTO_POP);
        for (unsigned i = 0; i < 11; i++)
            fprintf(chan_out, "%zu,", sim->population[i]);
        fprintf(chan_out, "\n");
        fflush(chan_out);

        // Reports the generation
        fprintf(chan_out, "%c %zu\n", PROTO_GENERATION, sim->generation);
        fflush(chan_out);

        // Have some individuals emigrate
        // TODO maybe non-constant probability ?
        for (int i = 0; i < 11; i++) {
            size_t emigrants = 0;
            for (size_t x = 0; x < sim->population[i]; x++)
              if ((double) rand()/(double) RAND_MAX < EMIGRATION_PROBABILITY)
                emigrants++;
            if (emigrants) {
                sim->population[i] -= emigrants;
                fprintf(chan_out, "%c %i %zu\n", PROTO_MIGRATE, i, emigrants);
                fflush(chan_out);
            }
        }

        // Inform the server we are done reporting
        fprintf(chan_out, "%c DONE\n", PROTO_DONE);
        fflush(chan_out);

        // Listen to server answer if any
        header = 0;
        while (header != PROTO_DONE) {
            fscanf(chan_in, "%c %m[^\n]%c", &header, &args, &eol);
            switch (header) {
              case PROTO_MIGRATE: {
                  unsigned strat = 0;
                  size_t count = 0;
                  sscanf(args, "%u %zu", &strat, &count);
                  sim->population[strat] += count;
                  break;
              }
              case PROTO_DONE: {
                  simulation_reset_points(sim);
              }
            }
            free(args);
        }

        // Advance in the simulation by one generation
        simulation_iter(sim);

        // Print a report meter to show something is going on
        printf(".");
        fflush(stdout);

        // Wait before the next cycle
        usleep(CLIENT_TICK);
    }

    // Inform the server we're closing the connection
    fprintf(chan_out, "%c CLOSE\n", PROTO_CLOSE);
    fflush(chan_out);

    // Close the channels and the socket
    fclose(chan_in);
    fclose(chan_out);
    close(socket);

    // Deallocate the simulation instance
    simulation_delete(sim);
}



// CONNECTION

int connect_to_server (char *hostname, int port) {

    // Create a TCP socket
    int s = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP);

    // Get the IP adress of the server
    struct hostent *host_address = gethostbyname(hostname);
    if (!host_address) {
        fprintf(stderr,"Erreur : serveur %s inconnu\n",hostname);
        exit(EHOSTUNREACH);
    }

    // Create the `connect` argument structure
    struct sockaddr_in server_address;
    server_address.sin_family = AF_INET;
    memcpy(&server_address.sin_addr,
            host_address->h_addr_list[0],
            host_address->h_length);
    server_address.sin_port = htons(port);

    // Connect to the server
  	printf("Connexion au serveur à %s:%d ...\n", hostname,port);
  	while (1) {
      	if (connect(s, (struct sockaddr *) &server_address, sizeof(server_address)) == 0) {
        		printf("Connecté !\n");
        		return s;
      	}
      	sleep(3);
      	printf("J'essaie à nouveau...\n");
  	}

}



// MAIN

int main(int argc, const char** argv) {

    // Parse CLI arguments
    struct arguments args;
    args.id = "<noid>"; // TODO: better default id
    args.initial_pop = 200;
    args.weights = weights_new(0, 3, 1, 5);
    args.disable = 0;
    argp_parse(&argp, argc, (char**) argv, 0, 0, &args);

    // Initialise pseudo-random number generator
    srand(time(NULL)); srand(rand());

    // Connect to the server
    int socket = connect_to_server(args.server, (int) args.port);

    // Make sure we catch Ctrl+C to properly close the connection
    signal(SIGINT, intHandler);

    // launch the simulation
    simulate(socket, args.id, args.initial_pop, args.weights, args.disable);
    return 0;
}

#include <stdarg.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>

#define PROTO_DISABLED 'd'
#define PROTO_GENERATION 'G'
#define PROTO_REGISTER 'R'
#define PROTO_POP 'P'
#define PROTO_POINTS 'S'
#define PROTO_MIGRATE 'M'
#define PROTO_CLOSE 'c'
#define PROTO_DONE 'D'

#define EMIGRATION_PROBABILITY 0.05
#define CLIENT_TICK 1000000/30
#define RENDER_TICK 1000000/24
#define TRSFER_TICK 1000000/2



#ifndef __GNU_SOURCE
#define GUESS 100

  // Backport of asprintf, to avoid using GNU_SOURCE
  int vasprintf(char **s, const char *fmt, va_list ap) {
  	va_list ap2;
  	char* a;
  	int l = GUESS;

  	if (!(a = (char*) malloc(GUESS))) return -1;

  	va_copy(ap2, ap);
  	l = vsnprintf(a, GUESS, fmt, ap2);
  	va_end(ap2);

  	if (l < GUESS) {
  		char* b = (char*) realloc(a, l+1U);
  		*s = b ? b : a;
  		return l;
  	}
  	free(a);
  	if (l < 0 || !(*s = (char*) malloc(l+1U))) return -1;
  	return vsnprintf(*s, l+1U, fmt, ap);
  }

  int asprintf(char** s, const char* fmt, ...) {
  	va_list ap;
  	va_start(ap, fmt);
  	int ret = vasprintf(s, fmt, ap);
  	va_end(ap);
  	return ret;
  }

#endif




int isdigitstr(const char* string) {
    for (int i = 0; string[i] != 0; i++) {
        if (!isdigit(string[i])) return 0;
    }
    return 1;
}

void read_csv(char* src, size_t* dest) {
    char *buff, *next;
    buff = next = malloc(sizeof(char)*strlen(src));
    for (unsigned i = 0; i < 11; i++) {
        sscanf(src, "%zu,%s", &dest[i], next);
        src = next;
    }
		free(buff);
}

unsigned randu(unsigned a, unsigned b) {
		return a + rand() % (b-a);
}

size_t sum(size_t* array, size_t len) {
		size_t sum = 0;
		while (len)
				sum += array[--len];
		return sum;
}

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <unistd.h>
#include <ctype.h>
#include <time.h>
#include <netinet/in.h>
#include <errno.h>
#include <string.h>
#include <pthread.h>
#include <signal.h>
#include <sys/time.h>

#include "table.h"
#include "ecology.h"

#include "common.h"


// Data about a single connected client
typedef struct {
    // id of the client
    char* id;
    // hostname of the client
    char* hostname;
    // port the client is listening on
    unsigned port;
    // client simulaiton detail
    Simulation* sim;
    // channels to communicate with the client
    FILE *in, *out;
    // client-relative migration data
    size_t incoming[11];
    size_t outgoing[11];
    // client-relative mutex to avoid concurrent modification
    pthread_mutex_t mutex;
} Client;

// Linked list to store currently connected clients
struct ClientLink {
    Client* client;
    struct ClientLink* next;
};

static struct {
    size_t length;
    struct ClientLink* head;
    pthread_mutex_t mutex;
} clients;

// Implementation © Linus Torvalds "Good coding tastes"
static void clients_append(Client* c) {
    pthread_mutex_lock(&clients.mutex);

    struct ClientLink** new_link = &clients.head;
    while (*new_link != NULL)
        new_link = &((*new_link)->next);

    *new_link = (struct ClientLink*) malloc(sizeof(struct ClientLink));
    (*new_link)->client = c;
    (*new_link)->next = NULL;

    clients.length++;

    pthread_mutex_unlock(&clients.mutex);
}

// Implementation © Linus Torvalds "Good coding tastes"
static void clients_remove(Client* c) {
  pthread_mutex_lock(&clients.mutex);

  struct ClientLink** indirect = &(clients.head);

  while ((*indirect)->client != c)
      indirect = &((*indirect)->next);

  struct ClientLink* link = *indirect;
  *indirect = (*indirect)->next;
  free(link);
  clients.length--;

  pthread_mutex_unlock(&clients.mutex);
}

// Get a client by index
static Client* clients_get(unsigned index) {
    struct ClientLink** indirect = &(clients.head);
    for (unsigned i = 0; i < index; i++)
        indirect = &((*indirect)->next);
    return (*indirect)->client;
}



static unsigned ACTIVE = 1;
void intHandler() {ACTIVE = 0;}



// Terminal-UI

const char* HEADERS[] = {
	"ID", "Generation",
  "Gent.", "Mech.", "DnDn.", "Ranc.",
	"PeriodM.", "PeriodG.", "MajMou.", "Mef.",
	"MajDur.", "Sond.", "DnDnDur.", NULL
};

void report_tui(void) {
    char* ROW[14];
    size_t CELL_SIZE = 12;

    while (ACTIVE) {

      // Reset terminal (I wanted to do it with ncurses but I didn't have enough
      // time on my hands, so the UI reduces to just one ANSI terminal code).
      printf("\033[1;0f"); fflush(stdout);

      print_table_headers(HEADERS, CELL_SIZE);

      for (struct ClientLink* link = clients.head; link != NULL; link = link->next) {
          Client* client = link->client;

          // Lock the client mutex before reading values
          pthread_mutex_lock(&client->mutex);

          // Prepare the row strings
          ROW[0] = client->id;
          asprintf(&ROW[1], "%zu", client->sim->generation);
          ROW[13] = NULL;
          for (int i = 0; i < 11; i++) {
              if (!simulation_is_disabled(client->sim, i))
                  asprintf(&ROW[i+2], "%zu", client->sim->population[i]);
              else
                  asprintf(&ROW[i+2], "N/A");
          }

          // Release the client mutex
          pthread_mutex_unlock(&client->mutex);

          // Print the row
          print_table_row((const char**) ROW, CELL_SIZE);

          // Free the allocated strings
          for (unsigned i = 1; i < 13; i++) free(ROW[i]);
      }

      // Print bottom plus another line to clean the last line
      print_table_bottom(13, CELL_SIZE);
      printf("%*s\n", 15*((int) CELL_SIZE), "");

      // DEBUG: Check that total hypervised population stays constant (works OK-ish)
      // size_t totalpop = 0;
      // size_t totalin = 0;
      // size_t totalout = 0;
      //
      // for (struct ClientLink* link = clients.head; link != NULL; link = link->next) {
      //     totalpop += sum(link->client->sim->population, 11);
      //     totalin += sum(link->client->incoming, 11);
      //     totalout += sum(link->client->outgoing, 11);
      // }
      //
      // printf("\tin cities: %10zu\n", totalpop);
      // printf("\t incoming: %10zu\n", totalin);
      // printf("\t outgoing: %10zu\n", totalout);
      // printf("\t    TOTAL: %10zu\n", totalpop+totalin+totalout);

      usleep(RENDER_TICK);
    }

}

void report_csv(void) {
  char* POINTS[14] = {0};
  char* POP[14] = {0};

  while (ACTIVE) {

    for (struct ClientLink* link = clients.head; link != NULL; link = link->next) {
        Client* client = link->client;

        // Lock the client mutex before reading values
        pthread_mutex_lock(&client->mutex);

        // Make points row
        asprintf(&POINTS[0], "%s:points", client->id);
        asprintf(&POINTS[1], "%zu", client->sim->generation);
        for (unsigned i = 0; i < 11; i++)
            asprintf(
              &POINTS[i+2],
              simulation_is_disabled(client->sim, i) ? "-1" : "%zu",
              client->sim->points[i]
            );

        // Make population row
        asprintf(&POP[0], "%s:population", client->id);
        asprintf(&POP[1], "%zu", client->sim->generation);
        for (unsigned i = 0; i < 11; i++)
            asprintf(
              &POP[i+2],
              simulation_is_disabled(client->sim, i) ? "-1" : "%zu",
              client->sim->population[i]
            );

        // Release the client mutex
        pthread_mutex_unlock(&client->mutex);

        // Print data rows
        print_csv_row((const char**) POINTS);
        print_csv_row((const char**) POP);

        // Free the allocated strings
        for (unsigned i = 0; i < 13; i++) {
          free(POINTS[i]);
          free(POP[i]);
        }
    }

    fflush(stdout);

    // Print bottom plus another line to clean the last line
    usleep(RENDER_TICK);
  }
}




// SIMULATION

void supervise(int client_socket) {
    int active = 1;

    Client client;
    pthread_mutex_init(&client.mutex, NULL);
    clients_append(&client);

    // Open r/w channels to the client
    pthread_mutex_lock(&client.mutex);
    client.in = fdopen(client_socket, "r");
    client.out = fdopen(client_socket, "w");
    client.id = "";
    client.hostname = "";
    weights_t w = weights_new(0, 0, 0, 0);
    client.sim = simulation_new(0, &w);
    pthread_mutex_unlock(&client.mutex);

    char header, eol, *args;
    while (active && ACTIVE) {

          // Read a message from the client
          if (fscanf(client.in, "%c %m[^\n]%c", &header, &args, &eol) < 3)
              continue;

          // DEBUG
          //printf("Received %c %s\n", header, args);

          switch (header) {
              // The client is introducing itself
              case PROTO_REGISTER: {
                  pthread_mutex_lock(&client.mutex);
                  sscanf(args, "%m[^@]@%ms", &client.id, &client.hostname);
                  pthread_mutex_unlock(&client.mutex);
                  break;
              }
              // The client claims which strategies are disabled
              case PROTO_DISABLED: {
                  pthread_mutex_lock(&client.mutex);
                  sscanf(args, "%u", &client.sim->disabled);
                  pthread_mutex_unlock(&client.mutex);
                  break;
              }
              // The client sends its generation
              case PROTO_GENERATION: {
                  pthread_mutex_lock(&client.mutex);
                  sscanf(args, "%zu", &client.sim->generation);
                  pthread_mutex_unlock(&client.mutex);
                  break;
              }
              // The client sends the state of its population
              case PROTO_POP: {
                  pthread_mutex_lock(&client.mutex);
                  read_csv(args, client.sim->population);
                  pthread_mutex_unlock(&client.mutex);
                  break;
              }
              // The client sends the earned points
              case PROTO_POINTS: {
                  pthread_mutex_lock(&client.mutex);
                  read_csv(args, client.sim->points);
                  pthread_mutex_unlock(&client.mutex);
                  break;
              }
              // The client is done sending what they want to send
              case PROTO_DONE: {
                  // Send migration data
                  pthread_mutex_lock(&client.mutex);
                  for (unsigned i = 0; i < 11; i++) {
                      if ((client.incoming[i])) {
                          fprintf(client.out, "%c %u %zu\n", PROTO_MIGRATE, i, client.incoming[i]);
                          client.incoming[i] = 0;
                      }
                  }
                  pthread_mutex_unlock(&client.mutex);
                  // The server is done as well
                  fprintf(client.out, "%c DONE\n", PROTO_DONE);
                  break;
              }
              // The client is sending migration data
              case PROTO_MIGRATE: {
                  unsigned strat = 0;
                  size_t count = 0;
                  pthread_mutex_lock(&client.mutex);
                  sscanf(args, "%u %zu", &strat, &count);
                  client.outgoing[strat] += count;
                  pthread_mutex_unlock(&client.mutex);
                  break;
              }
              // The client is closing the connection
              case PROTO_CLOSE: {
                  active = 0;
                  break;
              }
              // Unknown header
              default: {
                  break;
              }
          }

          fflush(client.in);
          fflush(client.out);
          free(args);
    }

    fclose(client.in);
    fclose(client.out);
    close(client_socket);
    free(client.id);
    free(client.hostname);
    simulation_delete(client.sim);
    clients_remove(&client);
    pthread_mutex_destroy(&client.mutex);

}

void manage_transfers() {
    Client *src, *dst;

    while (ACTIVE) {

      for (struct ClientLink* cl = clients.head; cl != NULL; cl = cl->next) {
          src = cl->client;

          // Acquire the client before starting transfers
          pthread_mutex_lock(&src->mutex);

          // Strat the migration
          for (unsigned i = 0; i < 11; i++) {
              while ((src->outgoing[i])) {
                  // Get a random destination where the strategy is allowed
                  dst = clients_get(randu(0, clients.length));
                  if (simulation_is_disabled(dst->sim, i)) continue;
                  // Transfer the outgoing individuals to their destination
                  if (src != dst) pthread_mutex_lock(&dst->mutex);
                  dst->incoming[i] += src->outgoing[i];
                  src->outgoing[i] = 0;
                  if (src != dst) pthread_mutex_unlock(&dst->mutex);
              }
          }

          // Release the client
          pthread_mutex_unlock(&src->mutex);

      }

      usleep(TRSFER_TICK);
   }
}


// CONNECTION

void serve_forever(int port) {

    size_t thread_count = 0;
    size_t thread_stack_size = 64;
    pthread_t* threads = calloc(0, sizeof(pthread_t));

    // Create a TCP socket
    int s = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP);

    // Bind the socket to the chosen port
    struct sockaddr_in server_address;
    server_address.sin_family = AF_INET;
    server_address.sin_addr.s_addr = INADDR_ANY;
    server_address.sin_port = htons(port);
    if (bind(s, (struct sockaddr*) &server_address, sizeof(server_address))) {
        fprintf(stderr,"Erreur : bind échoue, port %d encore occupé ?\n", port);
        exit(EBUSY);
    }

    // Put the socket in listen mode
    listen(s,1);

    // Set a timeout
    struct timeval tv;
    tv.tv_sec = 1;
    tv.tv_usec = 0;
    setsockopt (s, SOL_SOCKET, SO_RCVTIMEO, (char *) &tv, sizeof(tv));

    // Infinite loop (break with Ctrl+C), spawn a thread at every new connection
    while (ACTIVE) {

        // Accept a client connection
        int client_socket = accept(s, NULL, NULL);

        // Exit on error, continue on timeout
        if (client_socket == 0)
            continue;
        if (client_socket < 0) {
            if (errno == EINTR || errno == EWOULDBLOCK) continue;
            fprintf(stderr,"Erreur : connexion client échouée\n");
            exit(errno);
        }

        // FIXME: This is inefficient, I should allocate to 2*old_size instead
        if (thread_count > thread_stack_size - 2) {
            thread_stack_size *= 2;
            threads = realloc(threads, sizeof(pthread_t)*thread_stack_size);
        }
        pthread_create(&threads[thread_count++], NULL, (void* (*)(void *)) supervise, (void*) (long long int) client_socket);
    }

    // Close the socket
    close(s);

    // Join and deallocate the supervisor threads
    while (thread_count--)
        pthread_join(threads[thread_count], NULL);

    free(threads);

}


// MAIN

int main(int argc, const char** argv) {

    // Check argument count and arguments
    if ((argc < 2) || (argc > 3) || !isdigitstr(argv[1]) || (argc == 3 && strcmp(argv[2], "--force-ui"))) {
        printf("Usage: %s PORT [--force-ui]\n", argv[0]);
        printf("Gulag server -- supervise gulag clients.\n\n");
        printf("  --force-ui                 make sure to display the interface even when\n");
        printf("                             stdout is not a tty.\n");
        return argc > 1 ? (strcmp(argv[1], "--help") ? EINVAL : 0) : EINVAL;
    }

    // Parse the argument (argv[1] was sanitized earlier)
    int port = atoi(argv[1]);
    bool show_ui = ((argc == 3) || isatty(STDOUT_FILENO));

    // Initialise global clients collection mutex
    pthread_mutex_init(&clients.mutex, NULL);

    // Initialise pseudo-random number generator
    srand(time(NULL)); srand(rand());

    // Report thread: a pretty TUI if STDOUT is a tty, a csv log otherwise
    pthread_t report_thread;
    pthread_create(&report_thread, NULL, (void* (*)(void *)) (show_ui ? report_tui : report_csv), NULL);

    // Migration manager
    pthread_t mgr_thread;
    pthread_create(&mgr_thread, NULL, (void* (*)(void *)) manage_transfers, NULL);

    // Make sure we catch Ctrl+C to properly close the connection
    signal (SIGINT, intHandler);

    // Start serving the connexion
    serve_forever(port);

    // Deallocate the rendering and migration threads
    pthread_join(report_thread, NULL);
    pthread_join(mgr_thread, NULL);

    // Deallocate the mutex
    pthread_mutex_destroy(&clients.mutex);

    return 0;
}
